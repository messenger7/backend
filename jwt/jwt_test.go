package jwt_test

import (
	"testing"
	"time"

	jwtpkg "github.com/golang-jwt/jwt"
	"gitlab.com/Messenger7/backend/jwt"
)

func TestNewWithClaimsAndFrom(t *testing.T) {
	tests := []jwt.Claims{
		{
			UserID:         1,
			Type:           "access",
			StandardClaims: jwtpkg.StandardClaims{IssuedAt: time.Now().Unix(), ExpiresAt: time.Now().Add(time.Hour).Unix()},
		},
		{
			UserID:         2,
			Type:           "refresh",
			StandardClaims: jwtpkg.StandardClaims{IssuedAt: time.Now().Unix(), ExpiresAt: time.Now().Add(time.Hour * 24).Unix()},
		},
	}

	expected := []struct {
		UserID int64
		Type   string
		jwtpkg.StandardClaims
	}{
		{
			UserID:         1,
			Type:           "access",
			StandardClaims: jwtpkg.StandardClaims{IssuedAt: time.Now().Unix(), ExpiresAt: time.Now().Add(time.Hour).Unix()},
		},
		{
			UserID:         2,
			Type:           "refresh",
			StandardClaims: jwtpkg.StandardClaims{IssuedAt: time.Now().Unix(), ExpiresAt: time.Now().Add(time.Hour * 24).Unix()},
		},
	}

	for i, test := range tests {
		token, err := jwt.NewWithClaims(test).SignedString([]byte("secret"))
		if err != nil {
			t.Errorf("Test %d: unexpected error: %v", i, err)
		}

		parsedToken, err := jwtpkg.Parse(token, func(token *jwtpkg.Token) (interface{}, error) {
			return []byte("secret"), nil
		})
		if err != nil {
			t.Errorf("Test %d: unexpected error: %v", i, err)
		}

		claims := jwt.From(parsedToken)

		if claims.UserID != expected[i].UserID {
			t.Errorf("UserID is not equal. Expected: %d, Actual: %d", expected[i].UserID, claims.UserID)
		}

		if claims.Type != expected[i].Type {
			t.Errorf("Type is not equal. Expected: %s, Actual: %s", expected[i].Type, claims.Type)
		}

		if claims.IssuedAt != expected[i].IssuedAt {
			t.Errorf("IssuedAt is not equal. Expected: %d, Actual: %d", expected[i].IssuedAt, claims.IssuedAt)
		}

		if claims.ExpiresAt != expected[i].ExpiresAt {
			t.Errorf("ExpiresAt is not equal. Expected: %d, Actual: %d", expected[i].ExpiresAt, claims.ExpiresAt)
		}

	}
}

func BenchmarkNewWithClaimsAndFrom(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := jwt.NewWithClaims(jwt.Claims{
			UserID: 1,
			Type:   "access",
		}).SignedString([]byte("secret"))
		if err != nil {
			b.Errorf("Test %d: unexpected error: %v", i, err)
		}
	}
}
