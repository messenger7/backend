package jwt

import (
	"github.com/golang-jwt/jwt"
	"github.com/mitchellh/mapstructure"
)

func NewWithClaims(claims Claims) *jwt.Token {
	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
}

func From(v interface{}) Claims {
	mc := v.(*jwt.Token).Claims.(jwt.MapClaims)

	var claims Claims
	claims.StandardClaims = parseStandardClaims(mc)
	err := mapstructure.Decode(mc, &claims)
	if err != nil {
		return Claims{}
	}

	return claims
}

func parseStandardClaims(mc jwt.MapClaims) jwt.StandardClaims {
	claims := jwt.StandardClaims{}

	// TODO: sort
	if v, ok := mc["exp"]; ok {
		claims.ExpiresAt = int64(v.(float64))
	}
	if v, ok := mc["iss"]; ok {
		claims.Issuer = v.(string)
	}
	if v, ok := mc["iat"]; ok {
		claims.IssuedAt = int64(v.(float64))
	}
	if v, ok := mc["nbf"]; ok {
		claims.NotBefore = int64(v.(float64))
	}
	if v, ok := mc["sub"]; ok {
		claims.Subject = v.(string)
	}
	if v, ok := mc["jti"]; ok {
		claims.Id = v.(string)
	}
	if v, ok := mc["aud"]; ok {
		claims.Audience = v.(string)
	}

	return claims
}

type Claims struct {
	jwt.StandardClaims
	UserID int64  `json:"id,omitempty" mapstructure:"id"`
	Type   string `json:"type,omitempty" mapstructure:"type"`
}
