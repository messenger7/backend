package handler

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Messenger7/backend/storage"
)

type Handler struct {
	*storage.Storage
}

func New(h *Handler) *Handler {
	return h
}

type Service interface {
	REGISTER(h Handler, group *echo.Group)
}

func (h Handler) Register(group *echo.Group, service Service) {
	service.REGISTER(h, group)
}
