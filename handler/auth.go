package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4/middleware"

	jwtpkg "github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/Messenger7/backend/jwt"
	"gitlab.com/Messenger7/backend/storage"
)

type AuthService struct {
	Handler
	middleware.JWTConfig
}

func (s *AuthService) REGISTER(h Handler, group *echo.Group) {
	s.Handler = h

	group.POST("/login", s.Login)
	group.POST("/register", s.Register)
	group.POST("/refresh", s.Refresh)
}

type Tokens struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type LoginForm struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Login function gets data from LoginForm and binds it to form from context, then it calls srv.Storage.Users.ByPhone function where phone param is passed, then compares password from form and from function result, if it they not equal return http.StatusUnauthorized with ok:false json, if they equal generate a jwt token which will expire in 10 seconds
func (s *AuthService) Login(c echo.Context) error {
	var form LoginForm
	if err := c.Bind(&form); err != nil {
		return fmt.Errorf("handler/login: to bind a form: %w", err)
	}

	user, err := s.Storage.Users.ByEmail(form.Email)
	if err != nil {
		return fmt.Errorf("handler/login: to get user by email: %w", err)
	}

	if user.Password != form.Password {
		return c.JSON(http.StatusUnauthorized, map[string]bool{"ok": false})
	}

	accessClaims := jwt.Claims{UserID: user.ID, Type: "access", StandardClaims: jwtpkg.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour).Unix(),
		IssuedAt:  time.Now().Unix(),
	}}

	refreshClaims := jwt.Claims{UserID: user.ID, Type: "refresh", StandardClaims: jwtpkg.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		IssuedAt:  time.Now().Unix(),
	}}

	tokens, err := generateTokens(accessClaims, refreshClaims, s.SigningKey.([]byte))
	if err != nil {
		return fmt.Errorf("handler/login: %w", err)
	}

	sessionID, err := uuid.NewUUID()
	if err != nil {
		return fmt.Errorf("handler/login: to generate session id: %w", err)
	}

	tokensJSON, err := json.Marshal(tokens)
	if err != nil {
		return fmt.Errorf("handler/login: to marshal tokens: %w", err)
	}

	if err := s.Cache.Cache(sessionID.String(), tokensJSON, time.Hour*24); err != nil {
		return fmt.Errorf("handler/login: to cache tokens: %w", err)
	}

	// TODO: map below responses with wrong order
	return c.JSON(http.StatusOK, echo.Map{
		"ok":            true,
		"session_id":    sessionID,
		"access_token":  tokens.AccessToken,
		"refresh_token": tokens.RefreshToken,
	})
}

type RegisterForm struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

// Register registers a user
func (s *AuthService) Register(c echo.Context) error {
	form := &RegisterForm{}
	if err := c.Bind(&form); err != nil {
		return fmt.Errorf("handler/register: to bind a form: %w", err)
	}

	exists, err := s.Storage.Users.ByEmail(form.Email)
	if err != nil {
		return fmt.Errorf("handler/register: to get user by email: %w", err)
	}

	if exists != (storage.User{}) {
		return c.JSON(http.StatusBadRequest, echo.Map{"ok": false, "message": "this email already exists"})
	}

	u, err := s.Storage.Users.CreateUser(storage.User{
		FirstName: form.FirstName,
		LastName:  form.LastName,
		Email:     form.Email,
		Password:  form.Password,
	})
	if err != nil {
		return fmt.Errorf("handler/register: to create a user: %w", err)
	}

	return c.JSON(http.StatusCreated, echo.Map{
		"ok":      true,
		"message": u.ID,
	})
}

type RefreshForm struct {
	SessionID    string `json:"session_id"`
	RefreshToken string `json:"refresh_token"`
}

func (s *AuthService) Refresh(ctx echo.Context) error {
	form := &RefreshForm{}
	if err := ctx.Bind(&form); err != nil {
		return fmt.Errorf("handler/refresh: to bind a form: %w", err)
	}

	claimedTokensJSON, err := s.Cache.ByKey(form.SessionID)
	if err != nil {
		return fmt.Errorf("handler/refresh: to get tokens from cache: %w", err)
	}

	var claimedTokens Tokens
	if err := json.Unmarshal([]byte(claimedTokensJSON.(string)), &claimedTokens); err != nil {
		return fmt.Errorf("handler/refresh: to unmarshal tokens: %w", err)
	}

	if form.RefreshToken != claimedTokens.RefreshToken {
		return ctx.JSON(http.StatusUnauthorized, echo.Map{"ok": false, "message": "bad refresh token provided"})
	}

	refreshToken, err := jwtpkg.Parse(claimedTokens.RefreshToken, func(token *jwtpkg.Token) (interface{}, error) {
		return s.SigningKey, nil
	})
	if err != nil {
		return fmt.Errorf("handler/refresh: to parse a token: %w", err)
	}

	if !refreshToken.Valid {
		return ctx.JSON(http.StatusUnauthorized, echo.Map{"ok": false, "message": "refresh token is invalid"})
	}

	claimed := jwt.From(refreshToken)

	accessTokenSigned, err := jwt.NewWithClaims(jwt.Claims{UserID: claimed.UserID, Type: "access", StandardClaims: jwtpkg.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour).Unix(),
		IssuedAt:  time.Now().Unix(),
	}}).SignedString(s.SigningKey)
	if err != nil {
		return fmt.Errorf("handler/login: to generate access token: %w", err)
	}

	refreshTokenSigned, err := jwt.NewWithClaims(jwt.Claims{UserID: claimed.UserID, Type: "refresh", StandardClaims: jwtpkg.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		IssuedAt:  time.Now().Unix(),
	}}).SignedString(s.SigningKey)
	if err != nil {
		return fmt.Errorf("handler/login: to generate refresh token: %w", err)
	}

	tokens := Tokens{
		AccessToken:  accessTokenSigned,
		RefreshToken: refreshTokenSigned,
	}

	tokensJSON, err := json.Marshal(tokens)
	if err != nil {
		return fmt.Errorf("handler/login: to marshal tokens: %w", err)
	}

	if err := s.Cache.Cache(form.SessionID, tokensJSON, time.Hour*24); err != nil {
		return fmt.Errorf("handler/login: to cache tokens: %w", err)
	}

	return ctx.JSON(http.StatusOK, echo.Map{
		"ok":            true,
		"access_token":  tokens.AccessToken,
		"refresh_token": tokens.RefreshToken,
		"session_id":    form.SessionID,
	})
}

func generateTokens(accessClaims, refreshClaims jwt.Claims, secretKey []byte) (*Tokens, error) {
	accessToken, err := jwt.NewWithClaims(accessClaims).SignedString(secretKey)
	if err != nil {
		return nil, fmt.Errorf("handler/generateTokens: to generate access token: %w", err)
	}

	refreshToken, err := jwt.NewWithClaims(refreshClaims).SignedString(secretKey)
	if err != nil {
		return nil, fmt.Errorf("handler/generateTokens: to generate refresh token: %w", err)
	}

	return &Tokens{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}
