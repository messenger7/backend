package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"net/http"
	"strconv"

	jwtpkg "github.com/golang-jwt/jwt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/Messenger7/backend/jwt"
	"gitlab.com/Messenger7/backend/storage"
)

type UserService struct {
	Handler
	middleware.JWTConfig
}

func (s *UserService) REGISTER(h Handler, group *echo.Group) {
	s.Handler = h

	group.Use(middleware.JWTWithConfig(s.JWTConfig))

	group.GET("/:id", s.UserByID)
	// TODO: implement functions below
	// group.PATCH("/:id", s.UpdateUser)
	// group.DELETE("/:id", s.DeleteUser)
}

func (s *UserService) UserByID(ctx echo.Context) error {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		return fmt.Errorf("handler/userbyid: to parse id: %w", err)
	}

	token := ctx.Get("user").(*jwtpkg.Token)

	if err != nil {
		return fmt.Errorf("handler/userbyid: to parse token: %w", err)
	}

	if !token.Valid {
		return ctx.JSON(http.StatusUnauthorized, map[string]interface{}{"ok": false, "message": "invalid token"})
	}

	claimedToken := jwt.From(token)

	if claimedToken.Type != "access" {
		return ctx.JSON(http.StatusUnauthorized, map[string]interface{}{"ok": false, "message": "refresh token provided"})
	}

	sessionID := ctx.QueryParam("session_id")

	cachedTokensJSON, err := s.Cache.ByKey(sessionID)
	if err != nil {
		return fmt.Errorf("handler/userbyid: to get tokens from cache: %w", err)
	}

	var cachedTokens Tokens
	if err := json.Unmarshal([]byte(cachedTokensJSON.(string)), &cachedTokens); err != nil {
		return fmt.Errorf("handler/userbyid: to unmarshal tokens: %w", err)
	}

	if cachedTokens.AccessToken != token.Raw {
		return ctx.JSON(http.StatusUnauthorized, map[string]interface{}{"ok": false, "message": "invalid token for this session id"})
	}

	/*TODO: add additional checks: does not show his private data (email, phone, etc.)*/
	user, err := s.Users.ByID(id)
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return fmt.Errorf("handler/userbyid: to get user by id: %w", err)
	}

	if user == (storage.User{}) {
		return ctx.JSON(http.StatusNotFound, map[string]interface{}{"ok": false, "message": "user not found"})
	}

	if claimedToken.UserID == user.ID {
		return ctx.JSON(http.StatusOK, map[string]interface{}{"ok": true, "user": user})
	}

	chats, err := s.Chats.WithTargetID(claimedToken.UserID, user.ID)
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return fmt.Errorf("handler/userbyid: to get chats with target id: %w", err)
	}

	if user.DeletedAt.Valid {
		user.FirstName = "Deleted"
		user.LastName = "Account"
	}

	if len(chats) == 0 && !user.DisplayName.Valid {
		return ctx.JSON(http.StatusForbidden, map[string]interface{}{"ok": false, "message": "user is private"})
	}

	user = hidePrivateData(user)
	return ctx.JSON(200, user)
}

func hidePrivateData(user storage.User) storage.User {
	user.Model = gorm.Model{}
	user.Email = ""
	user.Telephone = ""
	return user
}
