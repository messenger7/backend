package storage

import (
	"github.com/go-redis/redis"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DBMSParams struct {
	// TODO: make DriverName usable
	DriverName string
	DataSource string
}

type CacheParams struct {
	Addr     string
	Password string
	DB       int
}

type Storage struct {
	*gorm.DB
	*redis.Client
	Chats       ChatsRepository
	Users       UsersRepository
	Attachments AttachmentsRepository
	Messages    MessagesRepository
	Cache       CacheRepository
}

func Open(s DBMSParams) (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(s.DataSource), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func OpenCache(s CacheParams) *redis.Client {
	return redis.NewClient(
		&redis.Options{
			Addr:     s.Addr,
			Password: s.Password,
			DB:       s.DB,
		})
}
