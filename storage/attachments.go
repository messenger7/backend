package storage

import (
	"gorm.io/gorm"
)

type (
	AttachmentsRepository interface {
		CreateAttachment(Attachment) (Attachment, error)
		ByID(Attachment) (Attachment, error)
		ByIDs([]int64) ([]Attachment, error)
	}

	Attachment struct {
		gorm.Model
		ID         int64  `json:"id" db:"id"`
		UploaderID int64  `json:"uploader_id" gorm:"uploader_id"`
		AbsPath    string `json:"abs_path" gorm:"abs_path"`
	}

	Attachments struct {
		*gorm.DB
	}
)

func (db *Attachments) CreateAttachment(attachment Attachment) (Attachment, error) {
	return attachment, db.Table("attachments").Create(&attachment).Error
}

func (db *Attachments) ByID(attachment Attachment) (Attachment, error) {
	return attachment, db.Table("attachments").Take(&attachment, "id = ?", attachment.ID).Error
}

func (db *Attachments) ByIDs(IDs []int64) (atts []Attachment, _ error) {
	return atts, db.Table("attachments").Find(&atts, "id IN (?)", IDs).Error
}
