package storage

import (
	"time"

	"gorm.io/gorm"
)

type (
	MessagesRepository interface {
		CreateMessage(Message) (Message, error)
		DeleteMessage(Message) error
		UpdateMessage(Message) (Message, error)

		// ByIDInChatID gets a message by his id in specific chat
		ByIDInChatID(messageID int64, chatID int64) (Message, error)
		// ByUserIDInChatID gets all messages by his user id in specific chat
		ByUserIDInChatID(userID int64, chatID int64) ([]Message, error)
		// ByDateFromToInChatID gets all messages by given date range in specific chat
		ByDateFromToInChatID(chatID int64, from time.Time, to time.Time) ([]Message, error)
	}

	Message struct {
		gorm.Model
		ID          int64        `json:"id" db:"id"`
		ChatID      int64        `json:"chat_id" db:"chat_id"`
		SenderID    int64        `json:"sender_id" db:"sender_id"`
		Body        string       `json:"body" db:"body"`
		Attachments []Attachment `json:"attachments" db:"attachments"`
	}

	Messages struct {
		*gorm.DB
	}
)

func (db *Messages) CreateMessage(m Message) (Message, error) {
	return m, db.Table("messages").Create(&m).Error
}

func (db *Messages) DeleteMessage(m Message) error {
	return db.Table("messages").Update("deleted_at", "now()").Error
}

func (db *Messages) UpdateMessage(m Message) (Message, error) {
	return m, db.Table("messages").Save(&m).Error
}

func (db *Messages) ByIDInChatID(messageID int64, chatID int64) (message Message, _ error) {
	return message, db.Table("messages").Find(&message, "id = ? AND chat_id = ?", messageID, chatID).Error
}

func (db *Messages) ByUserIDInChatID(userID int64, chatID int64) (messages []Message, _ error) {
	return messages, db.Table("messages").Find(&messages, "chat_id = ? AND sender_id = ?", chatID, userID).Error
}

func (db *Messages) ByDateFromToInChatID(chatID int64, from time.Time, to time.Time) (messages []Message, _ error) {
	return messages, db.Table("messages").Find(&messages, "chat_id = ? AND created_at BETWEEN ? AND ?", chatID, from, to).Error
}
