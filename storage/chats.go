package storage

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type (
	ChatsRepository interface {
		// CreateChat creates a new chat
		CreateChat(Chat) (Chat, error)
		// DeleteChat deletes chat by id (actually it's just marking as deleted)
		DeleteChat(Chat) error
		// UpdateChat updates chat's fields
		UpdateChat(Chat) (Chat, error)

		// ByID gets a chat by his id
		ByID(chatID int64) (Chat, error)
		// WithTargetID finds chats where userID and chatID are in the same group
		WithTargetID(userID, targetID int64) ([]Chat, error)
		// ByUserID finds chats where userID is a member
		ByUserID(userID int64) ([]Chat, error)
	}

	Chat struct {
		gorm.Model
		ID           int64         `json:"id" gorm:"id"`
		OwnerID      int64         `json:"owner_id" db:"owner_id"`
		Title        string        `json:"title" db:"title"`
		Participants pq.Int64Array `json:"participants" db:"participants" gorm:"type:bigint[]"`
	}

	Chats struct {
		*gorm.DB
	}
)

func (db *Chats) CreateChat(chat Chat) (Chat, error) {
	return chat, db.Table("chats").Create(&chat).Error
}

func (db *Chats) DeleteChat(c Chat) error {
	return db.Table("chats").Update("deleted_at", "now()").Error
}

func (db *Chats) UpdateChat(chat Chat) (Chat, error) {
	return chat, db.Table("chats").Save(&chat).Error
}

func (db *Chats) ByID(chatID int64) (chat Chat, _ error) {
	return chat, db.Table("chats").Take(&chat, "id = ?", chatID).Error
}

func (db *Chats) ByUserID(userID int64) (chats []Chat, _ error) {
	return chats, db.Table("chats").Find(&chats, "? = ANY (participants)", userID).Error
}

func (db *Chats) WithTargetID(userID, targetID int64) (chats []Chat, _ error) {
	return chats, db.Table("chats").Find(&chats, "? = ANY (participants) AND ? = ANY (participants)", userID, targetID).Error
}
