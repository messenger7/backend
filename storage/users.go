package storage

import (
	"database/sql"

	"gorm.io/gorm"
)

type (
	UsersRepository interface {
		CreateUser(User) (User, error)
		DeleteUser(User) error

		ByID(int64) (User, error)
		// ByPhone gets an account by his phone number
		ByPhone(string) (User, error)
		ByEmail(string) (User, error)
		ByDisplayName(string) (User, error)
	}

	User struct {
		gorm.Model
		ID          int64          `json:"id" db:"id"`
		DisplayName sql.NullString `json:"display_name" db:"display_name" gorm:"type:text, default:"`
		FirstName   string         `json:"first_name" db:"first_name"`
		LastName    string         `json:"last_name" db:"last_name"`
		Telephone   string         `json:"telephone,omitempty" db:"telephone"`
		Email       string         `json:"email,omitempty" db:"email"`
		Password    string         `json:"-" db:"password"`
	}

	Users struct {
		*gorm.DB
	}
)

func (db *Users) CreateUser(user User) (User, error) {
	return user, db.Table("users").Create(&user).Error
}

// DeleteUser marks user as deleted, does not delete him.
func (db *Users) DeleteUser(user User) error {
	return db.Table("users").Update("deleted_at", gorm.Expr("CURRENT_TIMESTAMP")).Where("id = ?", user.ID).Error
}

func (db *Users) ByID(id int64) (u User, err error) {
	return u, db.Unscoped().Table("users").Take(&u, "id = ?", id).Error
}

// ByPhone gets an account by his phone number
func (db *Users) ByPhone(number string) (u User, _ error) {
	return u, db.Unscoped().Table("users").Find(&u, "telephone = ?", number).Error
}

// ByEmail gets an account by his email
func (db *Users) ByEmail(email string) (u User, _ error) {
	return u, db.Unscoped().Table("users").Find(&u, "email = ?", email).Error
}

// ByDisplayName gets an account by his username
func (db *Users) ByDisplayName(display string) (u User, _ error) {
	return u, db.Unscoped().Table("users").Find(&u, "display_name = ?", display).Error
}
