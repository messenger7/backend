package storage

import (
	"time"

	"github.com/go-redis/redis"
)

type (
	CacheRepository interface {
		Cache(key string, value interface{}, ttl time.Duration) error
		KeyExists(key string) (bool, error)
		ByKey(key string) (interface{}, error)
		Delete(keys ...string) error
	}

	Caches struct {
		*redis.Client
	}
)

func (db *Caches) Cache(key string, value interface{}, ttl time.Duration) error {
	return db.Set(key, value, ttl).Err()
}

func (db *Caches) KeyExists(key string) (bool, error) {
	result, err := db.Exists(key).Result()
	if err != nil {
		return false, err
	}

	return result == 1, nil
}

func (db *Caches) ByKey(key string) (interface{}, error) {
	return db.Get(key).Result()
}

func (db *Caches) Delete(keys ...string) error {
	return db.Del(keys...).Err()
}
