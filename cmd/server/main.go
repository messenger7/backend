package main

import (
	"crypto/rand"
	"flag"
	"log"
	"os"
	"strconv"

	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/Messenger7/backend/handler"
	"gitlab.com/Messenger7/backend/storage"

	"github.com/labstack/echo/v4"
)

var verbose = flag.Bool("v", false, "verbose")

func main() {
	flag.Parse()
	e := echo.New()
	e.HTTPErrorHandler = func(err error, context echo.Context) {
		log.Println(err)
	}

	db, err := storage.Open(storage.DBMSParams{
		DriverName: os.Getenv("DB_DRIVER"),
		DataSource: os.Getenv("DB_DSN"),
	})
	if err != nil {
		log.Fatalf("to open database: %v", err)
	}

	dbNum, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		log.Fatalf("to convert redis db number to int: %v", err)
	}

	ch := storage.OpenCache(storage.CacheParams{
		Addr:     os.Getenv("REDIS_ADDR"),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       dbNum,
	})

	h := handler.New(&handler.Handler{Storage: &storage.Storage{
		DB:          db,
		Client:      ch,
		Users:       &storage.Users{DB: db},
		Chats:       &storage.Chats{DB: db},
		Messages:    &storage.Messages{DB: db},
		Attachments: &storage.Attachments{DB: db},
		Cache:       &storage.Caches{Client: ch},
	}})

	sKey := make([]byte, 10)
	n, err := rand.Read(sKey)
	if err != nil {
		log.Fatalf("to generate secret key: %v", err)
	}

	if *verbose {
		log.Printf("verbose: secretKey: written %d bytes\n", n)
	}

	jwtConfig := middleware.JWTConfig{SigningKey: sKey}

	v1 := e.Group("/api/v1")
	h.Register(v1.Group("/auth"), &handler.AuthService{Handler: *h, JWTConfig: jwtConfig})
	h.Register(v1.Group("/users"), &handler.UserService{Handler: *h, JWTConfig: jwtConfig})
	//h.Register(v1.Group("/messages"), &handler.MessageService{Handler: *h, SecretKey: sKey})
	//h.Register(v1.Group("/chats"), &handler.ChatService{Handler: *h, SecretKey: sKey})
	//h.Register(v1.Group("/attachments"), &handler.AttachmentService{Handler: *h, SecretKey: sKey})

	log.Fatalf("to start a server: %v", e.Start(os.Getenv("SRV_ADDR")))
}
